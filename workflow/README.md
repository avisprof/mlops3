# Snakemake

В задании был реализован [пайплайн](Snakefile) для обработки данных со следующими шагами:
* clean_data: очистка данных
* create_features_label_enc: создание новых признаков, используя `LabelEncoder`
* create_features_ohe: создание новых признаков, используя `OneHotEncoder`
* train_model_rf: обучение модели `RandomForestRegressor`
* train_model_lr: обучение модели `LinearRegression`

DAG пайплана snakemake выглядит следующим образом:
![dag.png](dag.png)

Установить `snakemake` можно с помощью следующей команды:
```conda install -c bioconda -c conda-forge snakemake```

Запустить  сборку проекта можно следующей командой (сборка запустится с ограничением на одном ядре и с ограничением на использование ресурсов в 100 Мб):
```snakemake -c1 --resources mem_mb=100```
