# Hydra

1. Создан файл с конфигурацией [config.yaml](config.yaml)

2. Пример использования конфигурации в файле [Snakemake](../workflow/Snakefile):

2.1. Инициализация:
```
from hydra import compose, initialize
initialize(version_base=None, config_path="../conf")
cfg = compose(config_name="config.yaml")
```

2.2. Чтение параметров
```
covid_start = cfg["preprocessing"]["start_date"],
covid_end = cfg["preprocessing"]["end_date"]
```
