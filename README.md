mlops3
==============================
## Project description

This is a project to predict a full year worth of sales for various fictitious learning modules from different fictitious Kaggle-branded stores in different countries.

Dataset was taken from Kaggle:
https://www.kaggle.com/competitions/playground-series-s3e19

This dataset is completely synthetic, but contains many effects of real-world data, e.g., weekend and holiday effect, seasonality, etc.

The task of this project is to predict sales during for year 2022


## Evaluation
Submissions are evaluated on SMAPE between forecasts and actual values.
SMAPE = 0 when the actual and predicted values are both 0.

## Submission File
For each id in the test set we need predict the corresponding num_sold.
The file should contain a header and have the following format:

id,num_sold
136950,100
136950,100
136950,100
etc.

## How to reproduce the project

Clone this repo into your local machine with the command:
`https://gitlab.com/avisprof/mlops3.git`

Run the following command to build docker image:
`docker build -t mlops3 .`

Run the following command to run the image:
`docker run -it -p 8080:8080 --rm mlops3`

Open the following URL in you browser:
`http://127.0.0.1:8080`



## Project Organization

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py


--------
