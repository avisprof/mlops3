# Contributing to MLOps3

Thank you for considering contributing to our project!

## Getting Started

1. Fork the repository
2. Clone the forked repository to your local machine.
3. Install dependencies `pip install -r requirements.txt`
4. Set up pre-commit hooks: `pre-commit install`
5. Make your changes.

## How to Contribute:
1. Create a new branch: `git checkout -b feature/my-feature`.
2. Make your changes and commit them: `git commit -am 'Add new feature'`.
3. You need to pass all the tests of `pre-commit` successfully:
![check_pre-commit_done](reports/figures/check_pre-commit_done.png)

If any test is failed, it will need to be corrected:
![check_pre-commit_wrong](reports/figures/check_pre-commit_wrong.png)

4. Push to the branch: `git push origin feature/my-feature`.
5. Submit a pull request.
