FROM python:3.9.19-slim

RUN pip install pipenv

WORKDIR /app

COPY ["Pipfile", "Pipfile.lock", "./"]

RUN pipenv install --system --dev

EXPOSE 8080

ENTRYPOINT [ "jupyter", "notebook", "--ip=0.0.0.0", "--port=8080", "--allow-root" ]
