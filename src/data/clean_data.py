import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
@click.argument('start_date', type=str)
@click.argument('end_date', type=str)
def clean_data(input_path: str, output_path: str, start_date: str, end_date: str):
	"""Function remove COVID data from dataset from start_date to end_date
	Args:
	    input_path (str): Path to read DataFrame
	    output_path (str): Path to save cleaned DataFrame
		covid_start (str): Start of COVID
		covid_end (str): End of COVID
	"""
	print(f'Read dataframe {input_path}')
	df = pd.read_csv(input_path, index_col=0, parse_dates=['date'])
	print(f'Remove COVID data from {start_date} to {end_date}')
	df_clean = df.query('date < @start_date or date > @end_date').copy()
	print(df_clean.columns)
	df_clean.to_csv(output_path, compression='zip')


if __name__ == '__main__':
	clean_data()
