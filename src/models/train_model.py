import pickle
import click

import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split

from hydra import compose, initialize

initialize(version_base=None, config_path='../../conf')
cfg = compose(config_name='config.yaml')

SEED = 42
TARGET = 'num_sold'
MODELS = {'rf': 'RandomForestRegressor', 'lr': 'LinearRegression'}


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
@click.argument('model_type', type=click.Choice(MODELS.keys()))
def train_model(input_path: str, output_path: str, model_type: str):
	"""Train model, evaluate model and save fitted model to the pickle file

	Args:
	    input_path (str): Path to read DataFrame
	    output_path (str): Path to save trained model
		model (str): Model to fit:
	                rf - RandomForestRegressor
	                lr - LinearRegression
	"""
	model_name = MODELS[model_type]
	print(f'Train {model_name}')

	X = pd.read_csv(input_path)
	X_train, X_val = train_test_split(X, shuffle=False, test_size=0.2)
	y_train = X_train.pop(TARGET).values
	y_val = X_val.pop(TARGET).values

	if model_type == 'rf':
		model = RandomForestRegressor(
			n_estimators=cfg['rf_params']['n_estimators'],
			max_depth=cfg['rf_params']['max_depth'],
			random_state=SEED,
		)
	else:
		model = LinearRegression()

	print(f'Fit {model_name} for evaluation')
	model.fit(X_train, y_train)

	y_pred = model.predict(X_val)
	score = mean_absolute_error(y_val, y_pred)
	print(f'Evaluate {model_name} by metric MAE: {score:.3f}')

	print(f'Fit {model_name} on full data')
	y = X.pop(TARGET)
	model.fit(X, y)

	print(f'Save {model_name} model to {output_path}')
	with open(output_path, 'wb') as f_out:
		pickle.dump(model, f_out)


if __name__ == '__main__':
	train_model()
