import datetime as dt

import click
import numpy as np
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
@click.argument('label_enc', type=click.BOOL)
@click.argument('ohe_enc', type=click.BOOL)
def create_features(input_path: str, output_path: str, label_enc: bool, ohe_enc: bool):
	"""Function create features for DataFrame
	Args:
	    input_path (str): Path to read DataFrame
	    output_path (str): Path to save DataFrame with new features
		label_enc (bool): If true - make label encoding
		ohe_enc (bool): If true - make one hot encoding
	"""
	print(f'Read dataframe {input_path}')
	df = pd.read_csv(input_path, index_col=0, parse_dates=['date'])
	print(f'Shape of data frame: {df.shape}')
	print('Create date features')
	df = create_date_features(df)
	print(f'Shape of data frame after create features: {df.shape}')

	if label_enc:
		print('Make label encondig')
		df = make_label_encoding(df)
		print(f'Shape of data frame after label encoding: {df.shape}')
	if ohe_enc:
		print('Make one hot encoding')
		df = make_one_encoding(df)
		print(f'Shape of data frame after one hot encoding: {df.shape}')

	del df['date']

	df.to_csv(output_path, compression='zip', index=False)


def create_date_features(df: pd.DataFrame) -> pd.DataFrame:
	"""Create features based on date
	Args:
		df (pd.DataFrame): Input dataframe
	Returns:
		pd.DataFrame: Output dataframe with date features
	"""
	df = df.copy()
	df['year'] = df['date'].dt.year - 2017
	df['month'] = df['date'].dt.month
	df['quarter'] = df['date'].dt.quarter
	df['day'] = df['date'].dt.day
	df['weekday'] = df['date'].dt.weekday
	df['dayofyear'] = df['date'].dt.dayofyear

	df['time_no'] = (df['date'] - dt.datetime(2017, 1, 1)) // dt.timedelta(days=1)
	df['year_sin_1'] = np.sin(np.pi * df['time_no'] / 182.5)
	df['year_cos_1'] = np.cos(np.pi * df['time_no'] / 182.5)
	df['year_sin_0.5'] = np.sin(np.pi * df['time_no'] / 365.0)
	df['year_cos_0.5'] = np.cos(np.pi * df['time_no'] / 365.0)

	return df


def make_label_encoding(df: pd.DataFrame) -> pd.DataFrame:
	"""Convert categorical features
	Args:
		df (pd.DataFrame):  Input dataframe
	Returns:
		pd.DataFrame:  Output dataframe with labels encoding
	"""
	cat_columns = ['country', 'store', 'product']
	for col in cat_columns:
		df[col] = df[col].astype('category')
		df[col] = df[col].cat.codes
	return df


def make_one_encoding(df: pd.DataFrame) -> pd.DataFrame:
	"""Encoding categorical features with one hot encoding

	Args:
		df (pd.DataFrame):  Input dataframe

	Returns:
		pd.DataFrame:  Output dataframe with one hot encoding
	"""
	cat_columns = ['country', 'store', 'product']
	df = pd.get_dummies(df, columns=cat_columns, dtype=np.int8)
	df.columns = df.columns.str.lower().str.replace(' ', '_')

	return df


if __name__ == '__main__':
	create_features()
